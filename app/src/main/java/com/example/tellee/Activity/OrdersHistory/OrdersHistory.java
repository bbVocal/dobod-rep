package com.example.tellee.Activity.OrdersHistory;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.tellee.R;

public class OrdersHistory extends AppCompatActivity {
    private ListView listView1;
    private String[] designPatterns;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_history);
        listView1 = (ListView) findViewById(R.id.listview);
        this.designPatterns = getResources().getStringArray(R.array.stringarry);

        // Create an array adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, designPatterns);
        listView1.setAdapter(adapter);



    }
}
