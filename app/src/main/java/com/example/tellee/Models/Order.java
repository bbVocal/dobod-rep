package com.example.tellee.Models;

public class Order {
    private String c_name;
    private  Product p1;
    private  Product p2;
    private  Product p3;
    private  Product p4;
    private  Product p5;
    private String date;


    public Order(String name, String c_name, Product p1, Product p2, Product p3, Product p4, Product p5, String date) {
        this.c_name = c_name;
        this.date = date;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
        this.p5 = p5;
    }

}
