package com.example.tellee.Models;

public class Product {
    private String name;
    private Integer price;
    private Integer order_count;
    private String imgUrl;
    private String description;

    public Product(String name, Integer price, Integer order_count, String imgUrl, String description) {
        this.name = name;
        this.price = price;
        this.order_count = order_count;
        this.imgUrl = imgUrl;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getDescription() {
        return description;
    }
    public  Integer getOrder_count(){
        return order_count;
    }
}
