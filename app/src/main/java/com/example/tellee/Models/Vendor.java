package com.example.tellee.Models;

public class Vendor {
    private String name;
    private String adress;
    private String visit_date;
    private String imgUrl;
    private String company_name;
    private Integer credit;
    private Integer dicount;

    public Vendor(String name, String adress, String visit_date, String imgUrl, String company_name, Integer credit, Integer dicount) {
        this.name = name;
        this.adress = adress;
        this.visit_date = visit_date;
        this.imgUrl = imgUrl;
        this.company_name = company_name;
        this.credit = credit;
        this.dicount = dicount;
    }

    public String getName() {
        return name;
    }

    public String getAdress() {
        return adress;
    }

    public String getVisit_date() {
        return visit_date;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getCompany_name() {
        return company_name;
    }

    public Integer getCredit() {
        return credit;
    }

    public Integer getDicount() {
        return dicount;
    }
}
